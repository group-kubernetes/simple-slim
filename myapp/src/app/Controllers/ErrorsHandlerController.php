<?php

namespace My\Controllers;

// use My\Models\User;
use Slim\Views\Twig as View;
use My\Models\ErrorsHandler;

class ErrorsHandlerController extends Controller
{

     public function error($request,$response){
          $ErrorsHandler=new ErrorsHandler();
          $ErrorsHandler->getError($request, $response
          ,$this->container
         );// ,$request->getParam('code')
          return $this->container->get('view')->render($response,'templates/contents/error1.html');
    }


}