<?php

namespace My\Controllers;

// use My\Models\User;
use Slim\Views\Twig as View;
use My\Models\Page1Modul;

class Page1Controller extends Controller
{

     public function sitemap($request,$response,$args){
     //   if(isset($args['myparam'])){

     //   }
     $myparam="";
     if(!empty($request->getQueryParams()['myparam'])){
          $myparam=$request->getQueryParams()['myparam'];
     }
          $home=new Page1Modul();
          $home->sitemap($request, $response,$this->container
                     ,$myparam);

     return $this->container->get('view')->render($response,'templates/pages/page1.html');

     }

}