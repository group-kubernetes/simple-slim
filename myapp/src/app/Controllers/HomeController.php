<?php

namespace My\Controllers;

// use My\Models\User;
use Slim\Views\Twig as View;
use My\Models\Home;

class HomeController extends Controller
{

     public function home($request,$response){
          $Home=new Home();
          $Home->getHome($request, $response
          ,$this->container
         );
          return $this->container->get('view')->render($response,'templates/Home.html');
    }


}