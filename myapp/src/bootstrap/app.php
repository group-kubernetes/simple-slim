<?php

use DI\ContainerBuilder;
use Slim\Factory\AppFactory;

use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

require __DIR__ . '/../../vendor/autoload.php';


//   Instantiate PHP-DI ContainerBuilder
$containerBuilder = new ContainerBuilder();

//  Set up settings
 $settings = require __DIR__ . '/settings.php';

$settings($containerBuilder);

$container = $containerBuilder->build();


// Instantiate the app
AppFactory::setContainer($container);



// Set view in Container
$container->set('view', function() {
  
   $view = Twig::create(__DIR__ . '/../views/',
       ['cache' => false]);
        
   return $view;
});

// Create App
$app = AppFactory::create();

// Register middleware
$app->addRoutingMiddleware();
$app->addBodyParsingMiddleware();
// $app->addErrorMiddleware(true, true, true);



#$app->setBasePath("/ex_slim4_7.4_php_simple/src/public"); // /myapp/api is the api folder (http://domain/myapp/api)
$BasePathx=$app->getBasePath();



// Add Twig-View Middleware
$app->add(TwigMiddleware::createFromContainer($app));


// Define Custom Error Handler
$customErrorHandler = function (
    Psr\Http\Message\ServerRequestInterface $request,
    \Throwable $exception,
    bool $displayErrorDetails,
    bool $logErrors,
    bool $logErrorDetails
) use ($app) {
    $response = $app->getResponseFactory()->createResponse();
    // seems the followin can be replaced by your custom response
    // $page = new Alvaro\Pages\Error($c);
    // return $page->notFound404($request, $response);
    return $response
    ->withHeader('Location', '/error')
    ->withStatus(404);
    $response->getBody()->write('not foundxxx');
    return $response->withStatus(404);
};




// Add Error Middleware
$errorMiddleware = $app->addErrorMiddleware(true, true, true);
// Register the handler to handle only  HttpNotFoundException
// Changing the first parameter registers the error handler for other types of exceptions
$errorMiddleware->setErrorHandler(Slim\Exception\HttpNotFoundException::class, $customErrorHandler);




// Get container
// $container = $app->getContainer();


// $container->set('notFoundHandler', function ($container) {
//     return function ($request, $response) use ($container) {
//         return $container['response']->withRedirect('http://localhost:8011/ex_slim4_7.4_php_simple/src/public/error');
//     };
// });

// start controller
$container->set('HomeController', function($container){
    return new \My\Controllers\HomeController($container);
});

$container->set('ErrorsHandlerController', function($container){
    return new \My\Controllers\ErrorsHandlerController($container);
});

$container->set('Page1Controller', function($container){
    return new \My\Controllers\Page1Controller($container);
});

// end controller

require __DIR__ . '/../app/routes.php';
