<?php

 
declare(strict_types=1);

// $dotenv = Dotenv\Dotenv::createImmutable('../');
// $dotenv->load();
// $dotenv->required(['DB_DRIVER', 'DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASSWORD']);

use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    // Global Settings Object
    $containerBuilder->addDefinitions([
        'settings' => [
            'determineRouteBeforeAppMiddleware' => false,
            'displayErrorDetails' => true, // Should be set to false in production
        ],
    ]);
};
